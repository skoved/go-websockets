package main

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func main() {
	router := gin.Default()
	router.LoadHTMLGlob("static/*")
	router.StaticFS("/scripts", http.Dir("scripts"))

	// don't trust all proxies
	router.SetTrustedProxies(nil)

	// set handlers
	router.GET("/", index)
	router.GET("/websocket", socket)
	router.Run(":8080")
}

func index(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "index.html", gin.H{})
}

func socket(ctx *gin.Context) {
	conn, err := upgrader.Upgrade(ctx.Writer, ctx.Request, nil)
	if err != nil {
		log.Printf("Error creating websocket connection: %s\n", err)
		return
	}
	defer conn.Close()

	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Printf("Error reading websocket message: %s\n", err)
			break
		}

		log.Printf("Received websocket message: %s\n", string(message))
	}
}
